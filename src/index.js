import apply from './apply.js';
import init from './init.js';
import templates from './templates.js';

export default {
    apply, init, templates
};