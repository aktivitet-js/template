import templates from './templates.js';

export default function apply( $el, template, options = {} ) {
    if ( templates.has( template ) )
        return templates.get( template )( $el, options );
};
