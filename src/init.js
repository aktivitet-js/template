import globals from "aktivitet/globals";
import apply from "./apply.js";

export default function init( sel = null, options = {} ) {
    if ( ! sel )
        sel = '[data-template]';

    var $$els = globals.$$( sel );

    $$els.forEach( function ( $el, i ) {
        apply( $el, $el.dataset.template, options );
    } );
};
